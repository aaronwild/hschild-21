These are my (probably extremely erroneous and also very provisional) lecture notes for the course [_Hochschild (Co)Homology_][1] taught by Dr. P. Belmans at the University of Bonn.
A compiled version can be found [here][2].


[1]: https://pbelmans.ncag.info/teaching/hh-2021
[2]: https://pankratius.gitlab.io/hochschild/hochschild.pdf
