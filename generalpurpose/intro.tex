\section*{Introduction}
These are my personal notes for the course \textit{Hochschild (Co)Homology}, held by Dr. P. Belmans at Bonn University in the summer term 2021.
You can find the current version here:
\begin{center}
  \Small
\url{https://pankratius.gitlab.io/hochschild/hochschild.pdf}\\
\end{center}
I use the following notation to indicate their current \emph{status} on the front page, sorted in decreasing likelihood of appearance:
\begin{itemize}
  \item \awfulstatuts{1} means that things are very bad --- don't ask, don't look, don't bother --- it's just \emph{bad}!
  \item  \badstatus{1} means that I've not come around to any meaningful proofreading of this vesion of the notes. So please do yourself a favour and don't take anything what you read here for granted (and maybe also please don't judge me for \sout{any} the many mistakes you might find).
  \item  \mediumstatus{1} means that I've already done some proofreading and fixing. Still, please be careful!
  \item  \okstatus{1} means that I'm somewhat confident that the notes don't have any major errors.
  \item  \donestatus{1} means that things are --- I can't believe it myself --- ok\emph{ish}... .
\end{itemize}
I'm really not sure how long I'll be able to keep up with this work --- so please get in touch if you want to contribute in any form.
What I do know is that I'm a master of erring massively, not only but particularly when doing math.
I apologize in advance, but please still let me know if you find stuff that's in need of improvement!
I've decided to use these notes as an excuse to learn more about Hochschild homology, so there's loads of stuff in here that wasn't covered in the lecture and with it probably a hell more of mistakes and inaccuracies.
A massive sorry if you got stuck on anything because I messed it up.
\par
In any case, this is my email-adress: \href{mailto:wild@uni-bonn.de}{wild@uni-bonn.de}. You can also use the \href{https://gitlab.com/pankratius/hochschild}{GitLab-repo} to report errors/add suggestions and content.
%\introsubsection{Notation and conventions}
%\subsection*{Conventions and notations}
%There is a list of symbols at the end of the document. In general, topological spaces (and CW-complexes) will be denoted by $X,Y,X',...$; simplicial sets by $S,T,S',...$ (note that this differs from the lecture), abelian groups by $A,B,A',...$ and the integers/rationals/.... in bold-face (i.e. $\zz$/$\rationals$/...). Ordinary categories are denoted cursive and usually by $\ccat,\dcat,...$; categories with a specific name have the first letter of their abbreviation written in cursive and the rest in the normal font\footnote{Everyone I know thinks this is hideous, but I just \emph{love} it.}, like $\topcat$, $\setcat$. One addition to this rule is that we denote the category of simplicial objects associated to a category $\ccat$ by $\ccat_{\IDelta}$ (the category $\sset$ of simplicial sets is of particular interest to us in this course.)\par
%Chain comlexes have a lower bullet and are denoted like $\chainc,\chaind,...$, cochain complexes have an upper bullet and are similarly denoted like $\cchainc,\cchaind,...$. In an arbitrary category $\ccat$, the morphisms between two objects $x$ and $y$ are denoted by $\ccat(x,y)$; if a category $\acat$ has some ``algebraic structure'' (say, being preadditive), we denote morphisms between $x$ and $y$ by $\hom_{\acat}(x,y)$ or simply, if there is no room for ambiguity, by $\hom(x,y)$. We'll use this especially freely in the case of $\abcat$.\par
%(Abstract) isomorphism are denoted by $\cong$ and that a specific map is an isomorphism is indicated by a tilde above the arrow: $\xrightarrow{\sim}$. For homotopy equivalences, we use $\simeq$ and $\xrightarrow{\simeq}$ respectivley.
\introsubsection{Plans for these notes}
(\emph{Hopefully, this will change over time})
\begin{itemize}
  \item Add some remark on notations and conventions.
  \item Fill all the missing gaps --- there are \emph{a lot} of them.
  \item Complete the list of symbols.
\end{itemize}
\newpage
\subsection{Outline of the course}
\emph{Author's note: This would be a lot better and more informative if I had a vague understanding of what's about to happen in this course. Sadly, I don't.}\par
We first want to consider Hochschild (co)homology of algebras.
Given an \emph{algebra} $A$ over a field $k$, as the name suggests, we are interested in the \emph{Hochschild homology} $\hoh(A)$ and \emph{Hochschild cohomology} $\coh(A)$ of $A$. These are certain invariants associated to $A$, and first of all we want to understand what they actually \emph{do}. A priori, both of them will be graded vector spaces, but there's more structure to be discovered. After having established this structure, we will use it for to apply Hochschild cohomology to \emph{deformation theory}. If $A$ is a commutatitve algebra, all of the above can be refined, and the applications to deformation theory become more geometrically. Maybe, we will also dive into \emph{cyclic homology} or more computational aspects (in the device-kind-of-sense).\par
In the second part, we will be interested in \emph{smooth (quasi-) projective varieties} over $k$, and how to ``extend'' Hochschild homology and cohomology to them. Again, we first want to find out what they actually measure and how to connect that to what we already will have learned when consindering deformation theory.
We want to establish the \emph{Hochschild-Konstant-Rosenberg decomposition}, which will help us understand Hochschild (co)homology in the case of varieties.\par
Culminating, the aim of the third part of the course is to see how everything we will have learned so far has its natural unification in the language of differentially graded categories, or \emph{dg categories} for short.
In particular, we want to see how working with dg categories helps resolve some issues around limited functoriality of Hochschild (co)homology.\par
Dr. Belmans now explained what the symbols in the two prominent equations on the cover of his notes mean:
\begin{align*}
  \defo_A(R)&\cong \MC\left(\idm \dtensor_k\ctemp(A)[1]\right)
  \\
  \hh^i(X)&\cong
  \bigoplus_{p+q=i}\hh^p\left(X,\exterior^q\tspace_X\right)
\end{align*}
Here:
\begin{itemize}
  \item $\defo_A(R)$ ``stands'' for \emph{deformation theory}.
  \item the right-hand side of the first equation involves \emph{differential graded Lie algebras}, which gives a more ``modern'' approach to deformation theory.
  \item $\hh^i(X)$ is the Hochschild cohomology of a quasi projective variety $X$, which is linked to \emph{Fourier-Mukaki-transforms} and the derived category of $X$. In particular, we will have a reason to study such derived categories.
  \item Finally, the right-hand side of the second equation is the result of the \emph{Hochschild-Konstant-Rosenberg decomposition}.
\end{itemize}
