\section{Structure on Hochschild (co)homology}
\subsection{Gerstenhaber algebra structure on $\coh(A)$}
\begin{defn}
  A \emph{Gerstenhaber algebra} is a $\zz$-graded vector space with
  \begin{enumerate}
    \item a graded-commutative product of degree zero; and
    \item a Lie bracket of degree -1;
  \end{enumerate}
  both of which are compatible via the \emph{Poisson identity}
  \[
  [a,bc] = [a,b]c+\left(-1\right)^{\left(|a|-1\right)|b|}b[a,c].
  \]
  Spelled out, these operations are required to satisfy:
  \begin{itemize}
    \item The product is of degree zero: $|ab|=|a|+|b|$.
    \item The bracket is of degree $-1$: $\abs{[a,b]} = \abs{a}+\abs{b}-1$.
    \item The product is assoicative: $(ab)c = a(bc)$.
    \item The product is graded-commutative: $ab = (-1)^{\abs{a}\abs{b}}ba$.
    \item The Lie bracket is graded-anticommutative:
    \[
    [a,b] = - (-1)^{(\abs{a}-1)(\abs{b}-1)}[b,a]
    \]
    \item The Lie bracket satisfies the graded Jacobi identity:
    \[
    [a,[bc]] = [[a,b],c] + (-1)^{(\abs{a}-1)(\abs{b}-1)}[b,[a,c]].
    \]
  \end{itemize}
\end{defn}
\begin{example}
  Let $\glie$ be a Lie algebra with bracket $[-,-]$. Then the exterior algebra
  \[
  \exterioralg \glie = \bigoplus_{i\geq 0} \exterior^i \glie
  \]
  becomes a Gerstenhaber algebra, by taking the usual graded-commutative structure on $\exterioralg$ as product and extending the Lie bracket
  \[
  [-,-]\mc \exterior^1\glie\tensor\exterior^1\glie \to  \exterior^1 \glie
  \]
  to all of $\exterioralg \glie$.
\end{example}

We now state and prove the first theorem on structure on Hochschild cohomoloy:
\begin{theorem}\label{structure_on_cohomology:hoh-is-gerstenhaber}
  There is a Gerstenhaber algebra structure on $\coh(A)$ such that there are isomophisms
  \begin{enumerate}
    \item of algebras: $\hh^0(A)\cong \centre(A)$ and
    \item of Lie algebras: $\hh^1(A)\cong\outder(A)$.
  \end{enumerate}
\end{theorem}
\begin{defn}
  Let $f\in \hohcompg^m$ and $g\in \hohcompg^m$. Then their \emph{cup product} is defined as
  \begin{align*}
    f\cupp g\mc A^{\tensor(n+m)}&\to M\\
    a_1\tensor\ldots\tensor a_{m+n}&\mapsto
    (-1)^{mn}f(a_1\tensor\ldots\tensor a_m)g(a_{m+1}\tensor\ldots\tensor a_{m+n}).
  \end{align*}
  \glsadd{cupproduct}
\end{defn}
Before we do something with the cup-product, we note the following:
\begin{construction}\label{structure_on_cohomology:dga-to-ga}
  \leavevmode
  \begin{enumerate}
    \item Let $\chainc$ be a differential graded module, i.e. a chain complex with differential
    \[
    d_n \mc A_n \to A_{n+1}.
    \]
    We say that $\chain{C}$ is a \emph{differential graded algebra} if:
    \begin{itemize}
      \item
      There is a chain map
    \[
      \mu\mc \chain{C}\tensor\chain{C}\to \chain{C},
    \]
    called the \emph{multiplication},
    that is associative in the sense that the compositions
    \[
    \begin{tikzcd}
      \chain{C}\tensor\chain{C}\tensor\chain{C}
      \ar{r}[above]{\mu\tensor \id}
      \ar{d}[left]{\id\tensor\mu}
      &
      \chain{C}\tensor \chain{C}
      \ar{d}[right]{\mu}
      \\
      \chain{C}\tensor\chain{C}
      \ar{r}[below]{\mu}
      &
      \chainc
    \end{tikzcd}
    \]
    agree; and
    \item There is a \emph{unit element} $1\in C_0$, that is unital in the sense that for every $c\in C_n$ it holds that
    \[
    \mu(1,c) = \mu(c,1) = 1.
    \]
    \end{itemize}
    \item Using the universal property of the tensor product (\cite[00P5]{kerodon}) of chain complexes, we see that this is the same as a graded ring $\chainc$ together with maps
    \[
    d_n\mc C_{n}\to C_{n+1}
    \]
    satisfying $d^2 = 0$ and the \emph{Leibnitz rule}:
    \[
    d_{m+n}(xy) = d_m(x)y + (-1)^{m}xd_n(y)
    \]
    holds for $x\in C_m$ and $y\in C_n$.
    \item If $\chainc$ is a differential graded algebra, then the graded module
    \[
    \hgraded(C) \defined \bigoplus_{n\in \zz}\hlgy_n(C)
    \]
    becomes a graded ring via the multiplication
    \[
    \hlgy_m(C)\times\hlgy_n(C)
    \to
    \hlgy_{m+n}(C),
    ~
    ([x],[y])\mapsto [xy].
    \]
    We need the Leibnitz rule to get well-definedness: For two cycles $x\in \zker_m(C)$ and $y\in \zker_n(C)$, we have that $xy$ is again a cyclce, as
    \[
    d(xy) = d(x)y + (-1)^mxd(y) = 0;
    \]
    now for a boundrary $d(x')$ for an $x'\in C_{m-1}$, we have
    \begin{align*}
    [(x+d(x'))y] &= [xy] + [d(x')y]\\
                        &= [xy] + [d(x')y] + (-1)^m[x'd(y)]
                        \\
                        &= [xy].
    \end{align*}
    \item From this description it's also immediate that if the multiplication in $C$ is (graded) commutative, the multiplication on $\hgraded(C)$ is too.
  \end{enumerate}
\end{construction}
\begin{prop}
  If we equip the graded module $\hohcompg(A)$ with the modified differential
  \[
  \partial \mc \hohcompg^n(A)\to \hohcompg^{n+1}(A),~f\mapsto \left(-1\right)^n f\circ d_{n+1},
  \]
  then it becomes a differential graded algebra under the cup product. (Note that this change of sign does not change the homology groups!)
\end{prop}
\begin{proof}
  The associativity is an immediate consequence from the associativity of the multiplication in $A$. That it satisfies the Leibnitz rule can be checked by direct computation.
\end{proof}
Using \cref{structure_on_cohomology:dga-to-ga}, we immediatley get:
\begin{cor}\label{structure_on_cohomology:hoh-is-ga}
  The Hochschild cohomology $\coh(A)$ is a graded algebra.
\end{cor}
Next up, we would like to see that this multiplication on $\coh(A)$ is indeed graded commutative. Similar to the case of singular cohomology, it is helpful to introduce some further structure first (which we'll need later anyways).
\begin{defn}
  For two elements $f\in \hohcompg^m(A)$ and $g\in\hohcompg^n(A)$, the element $f\circ_i g\in \hohcompg^{m+n-1}(A)$ is defined as
  \[
  \begin{tikzcd}[row sep = small]
  a_1\tensor \ldots \tensor a_{m+n-1}
  \ar[mapsto]{d}
  \\
  f\left(a_1\tensor\ldots\tensor a_{i-1}\tensor g(a_i\tensor\ldots \tensor a_{i+n-i})\tensor a_{i+n}\tensor \ldots \tensor a_{m+n-1}\right)
  \end{tikzcd}
  \]
  Then the \emph{circle product} of $f$ and $g$ is the element $f\circ g\in C^{m+n-1}(A)$ defined as
  \[
  f\circ g \defined \sum_{i=1}^{m}(-1)^{(i-1)(n+1)}f\circ_i g.
  \]
  \glsadd{circleproduct}
\end{defn}
Again, some remarks are in order:
\begin{construction}
\leavevmode
\begin{enumerate}
\item
  A differential graded module $\glie$ together with a degree-zero map of differential graded modules
  \[
  [-,-]\mc \glie\tensor \glie \to \glie
  \]
  called a \emph{bracket} is called \emph{differential graded Lie algebra} or \emph{dg Lie algebra} if the following holds:
  \begin{itemize}
  \item The bracket is \emph{skew-symmetric}, i.e.
  \[
  [x,y] = \left(-1\right)^{\abs{x}\abs{y}}[x,y]
  \]
  holds.
  \item The bracket satisfies the \emph{graded Jacobi identity}:
  \[
  [x,y,z] =  [[x,y],z] + (-1)^{\abs(x)\abs{y}}[y,[x,z]]
  \]
  \item The bracket and the differential are related in the following way:
  \[
  d([x,y]) = [dx,y] + (-1)^{\abs{x}}[x,d(y)].
  \]
\end{itemize}
\item Given a differential graded Lie algebra $\glie$, we can again form the graded homology module $\hgraded(\glie)$. Somewhat similar to the above, we can then show that it is a \emph{graded Lie algebra}, i.e. satisfies all the axioms of a differential graded Lie algebra that do not involve a differential.
\end{enumerate}
\end{construction}

\begin{prop}\label{structure_on_cohomology:hoh-is-graded-lie}
  Equip the complex $\hohcompg(A)$ with the differential $\partial$ and the bracket
  \[
  [f,g]\defined f\circ g - (-1)^{(\abs{f}-1)(\abs{g}-1)}.
  \]
  Then
  \begin{equation}\label{structure_on_cohomologe:bracket-relation}\tag{$\ast$}
  \partial([f,g]) = [\partial(f),g]+(-1)^{m-1}[f,\partial(g)]
  \end{equation}
  holds,
  and hence $\hohcompg(A)[1]$ becomes a differential graded Lie algebra and $\coh(A)[1]$ a graded Lie algebra.
\end{prop}
\begin{rem}
  Note that we need the shift because in \eqref{structure_on_cohomologe:bracket-relation}, there is an exponent of the form $m-1$.
\end{rem}
\begin{rem}
  The proof of \cref{structure_on_cohomology:hoh-is-graded-lie} is already awful when it comes to signs, and I'll not check it here. However, there is, apparently, one important trick involved: Denote by $\mu \mc A\tensor_k A\to A$ the multiplication map of the algebra $A$. Then this is an element of $\hohcompg^2(A)$, and the image of $\id\mc A\to A$ under the Hochschild differential. Now for every element $f\in \hohcompg^n(A)$, it holds that
  \[
  [f,\mu] = -d(f),
  \]
  where $d$ is the Hochschild differential (and not $\partial$). This again can be checked by direct calculation.
\end{rem}
As another tedious calculation lemma, we have
\begin{lem}
  Let $f\in \hohcompg^m(A)$ and $g\in \hohcompg^n(A)$ be Hochschild cochains. then
  \[
  f\cupp g - (-1)^{mn}g\cupp f = d(g)\circ f + (-1)^md(g\circ f) + (-1)^{m-1} g\circ d(f)
  \]
\end{lem}
\begin{prop}\label{structure_on_cohomology:coh-is-graded-commutative}
  The graded algebra $\coh(A)$ is graded-commutative, i.e.
  \[
  f\cupp g - (-1)^{mn} g\cupp f = 0
  \]
  holds for Hochschild cocylces $f\in \hohcompg^m(A)$ and $g\in \hohcompg^n(A)$.
\end{prop}
\begin{proof}
  This is immediate from the lemma above, since the first and third term on the right hand side vanish (since $f$ and $g$ are cocycles) and the term $d(g\circ f)$ becomes zero after taking homology.
\end{proof}
We now have (nearly) everything together for a:
\begin{proof}[Proof of \cref{structure_on_cohomology:hoh-is-gerstenhaber}]
  We have seen in \cref{structure_on_cohomology:hoh-is-ga} that the Hochschild cohomology $\coh(A)$ is a graded algebra and in \cref{structure_on_cohomology:coh-is-graded-commutative} that it is in fact graded commutative: in \cref{structure_on_cohomology:hoh-is-graded-lie} that $\coh(A)[1]$ is a graded Lie algebra.
  All that's missing is the Poisson identity, but this is another tedious calculation which I'll not even attempt (c.f. \cite[Thm. 5]{og-gerstenhaber}).
\end{proof}

\subsection{Another interpreation of the structure on $\coh(A)$}
\begin{numtext}
  It's a (suppossedly) well-known result that the set of equivalence classes of extensions of an $A$-module $M$ by an $A$-module $N$, that is the set of all short-exact sequences of $A$-modules
  \[
  0
  \to
  N
  \to
  X
  \to
  M
  \to
  0
  \]
  under the equivalence relation of the existence of a commutative diagram of the form
  \[
  \begin{tikzcd}
    0
    \ar{r}
    &
    N
    \ar[equal]{d}
    \ar{r}
    &
    X
    \ar{d}[left]{\cong}
    \ar{r}
    &
    M
    \ar[equal]{d}
    \ar{r}
    &
    0
    \\
    0
    \ar{r}
    &
    N
    \ar{r}
    &
    X'
    \ar{r}
    &
    M
    \ar{r}
    &
    0
  \end{tikzcd}
  \]
  form an abelian group under the \emph{Baer sum} and that they are as an abelian group naturally isomorphic to $\ext^1_A(M,N)$, c.f. \cite[3.4]{weibel}.
  The aim of the following is to:
  \begin{itemize}
    \item In a first step, we'll introduce the $n$-fold extension categories $\extcat_A^n(M,N)$ and the set of their connected components $\pi_0(\extcat_A^n(M,N))$. We will then see that there are group isomophisms
    \[
    \ext_A^n(M,N) \cong \pi_0\extcat_A^n(M,N),
    \]
    and that they recover what we already know in the case $n=1$. This is part of \emph{Retakh's Theorem}.
    \item In a second step, we will introduce a product structure on the homotopy category, which will turn out to correspond to the product on Hochschild cohomology.
    \item Finally, we will also introduce a loop operation on the homogotpy categories, which will give us the Lie bracket on Hochschild cohomology.
  \end{itemize}
\end{numtext}
All of this is mostly an (uninformed) summary of \cite{schwede-lie-algebra} and \cite{herman-monoidal}, where the latter works in the more general setting of exact $k$-categories.
\subsubsection{Yoneda extensions}
\begin{construction}
\leavevmode
  \begin{enumerate}
   \item Let $M$ and $N$ be left $A$-modules. We define the category $\extcat_A^n(M,N)$ of \emph{$n$-fold extensions of $M$ by $N$}:
   \begin{itemize}
      \item Objects in $\extcat^n(M,N)$ are given by exact sequences of $R$-modules
      \[
      E\defined
      0
      \to
      N
      \to
      E_{n-1}
      \to
      \ldots
      \to
      E_0
      \to
      M
      \to
      0.
      \]
      Note that in the case $n=1$, we recover the usual notion of extensions. In the case $n=0$, we define $\extcat^0(M,N)$ to be the discrete category with objects given by $\hom_A(M,N)$.
      \item Given two $n$-fold extensions $E$ and $F$ of $M$ with $n\geq 1$, a morphism $E\to F$ is a commutative diagram of the form
      \[
      \begin{tikzcd}[column sep = small]
        0
        \ar{r}
        &
        N
        \ar{r}
        \ar[equal]{d}
        &
        E_{n-1}
        \ar{r}
        \ar{d}
        &
        \ldots
        \ar{r}
        &
        E_0
        \ar{d}
        \ar{r}
        &
        M
        \ar[equal]{d}
        \ar{r}
        &
        0
        \\
        0
        \ar{r}
        &
        N
        \ar{r}
        &
        F_{n-1}
        \ar{r}
        &
        \ldots
        \ar{r}
        &
        F_0
        \ar{r}
        &
        M
        \ar{r}
        &
        0
      \end{tikzcd}
    \]
    Composition is given by composition on every level. Note that we don't require the maps $E_i\to F_i$ to have any special properties -- that they're isomorphisms in the case $n=1$ is simply a consequence of the 3-lemma. So in particular, we see that $\extcat^1(M,N)$ is a groupoid.
  \end{itemize}
  \item Let now $\chain{P}\to M$ be a projective resolution of $M$, i.e. in particular we have
  \[\ext^{\bullet}(M,N)\cong \hlgy_{\bullet}(\hom(P,N)).\]
  Given any $n$-cocylce $\pphi\mc P_n\to N$ of $\hom(P,N)$, we construct an $n$-fold extension $K(\pphi)$, i.e. an object of $\extcat^n(M,N)$, as follows:
  The module $K(\pphi)_{n-1}$ is given as the pushout:
  \[
  \begin{tikzcd}
    P_n
    \ar{r}[above]{\pphi}
    \ar{d}[left]{d}
    \ar[phantom]{rd}{\ulcorner}
    &
    N
    \ar[dashed]{d}
    \\
    P_{n-1}
    \ar[dashed]{r}
    &
    K(\pphi)_{n-1}
  \end{tikzcd}
  \]
  For $0\leq i<n-1$, we set $K(\pphi)_i \defined P_i$; the map $N\to K(\pphi)_{n-1}$ is the one from the construction of the pushout, the map \[K(\pphi)_{n-1}\to K(\pphi)_{n-2}\] from the universal property of the pushout applied to the maps $d\mc P_{n-1}\to P_{n-2}$ and $0\mc N\to P_{n-2}$; all other maps are the ones from the projective resolution of $M$ by $P$.
  Note that the map $N\to K(\pphi)_{n-1}$ is injective, so $K(\pphi)$ is indeed an extension.
  \item Given any category $\ccat$, the \emph{nerve} of $\ccat$ is the simplicial set $\nerve(\ccat)$ where $n$-simplices correspond to $n$-chains of composable morphisms in $\ccat$. It's fully faitful and part of an adjunction
  \[
  \htpycat\mc \sset\leftrightarrows \catcat\mcc \nerve,
  \]
  where $\htpycat(X)$ is called the \emph{homotopy category} of a simplicial set $X$.
  The nerve of any ordinary category is an $\infty$-category.
  \item
  If $X$ is any simplicial set, we denote by $\pi_0(X)$ the set of its connected components. For an ordinary category $\ccat$, we also write $\pi_0(\ccat)$ for the set of connected components of the simplicial set $\nerve(\ccat)$.
  \item We now want to show that the assignment $\pphi\mapsto K(\pphi)$ extends to a well-defined map
  \[
  K\mc \hlgy^n(\hom(P,N))\to \pi_0(\extcat^n(M,N)).
  \]
  For that, we first note that for any $\psi\mc P_{n-1}\to N$ we get a map
  \[
  \mu(\psi)\mc K(\pphi)\to K(\pphi+\psi\circ d),
  \]
  where
  \[
  \mu(\psi)_{n-1}\mc K(\pphi)_{n-1}\to K(\pphi+\psi\circ d)_{n-1}
  \]
  is induced by $(p,n)\mapsto (p,n-\psi(p))$.
  \end{enumerate}
\end{construction}
\subsubsection{The cup product as Yoneda product}
\subsubsection{The bracket as loops on extensions}
