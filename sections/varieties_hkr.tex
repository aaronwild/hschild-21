\section{HKR for varieties}
In this section, we call the sheaf $\kahlsx$ of differentials of a $k$-scheme $X$ the \emph{cotangent bundle} of $X$ and associate to it the sheaf
\[
\tspace_X \defined \shhom_X(\kahlsx,\ox),
\]
which we call the \emph{tangent bundle} of $X$.
We also keep our standing assumption that $k$ is a field of characteristic zero.
\glsadd{tangent-bundel}
\subsection{Statement and some examples}
\begin{theorem}
  Let $k$ be a field of characteristic zero, and let $X$ be a smooth separated scheme of fininte type over $k$.
  Then the $i$-th Hochschild cohomology of $X$ satisfies
  \begin{align*}
  \hh^i(X)
  &
  \cong \bigoplus_{p+q = i}\hlgy^p(X;\exterior^q \tspace_X);
  \intertext{the $i$-th Hochschild homology satisfies}
  \hh_i(X)
  &
  \cong
  \bigoplus_{q-p = i}\hlgy^q(X;\kahlsxp{p}).
  \end{align*}
\end{theorem}
\begin{example}
  For $X=\pp^n$, this recovers precisley what we calculated in \cref{varieties-defn:pn-example}.
  (Note that $(\Omega^q_X)^\vee \cong \exterior^q((\Omega^1)^{\vee})$ holds, since taking duals commutes with taking exterior powers for locally free modules over $k$.)
\end{example}
\begin{numtext}
  We now set out to prove the HKR-theorem for smooth varieties, following the approach of \cite{markarian-hkr}.
  They use the approach of Atiyah-classes and Lie algebra objects in $\bderivedd(X)$, so we need to establish some more machinery.
\end{numtext}
\subsection{Proof using Atiyah classes}
\begin{defn}
  Let $X$ be a smooth variety and $\she$ a vector bundle on $X$ (i.e. a locally free sheaf of finite rank).
  A \emph{connection} on $\she$ is a morphism of sheaves of vector spaces
  \[
  D
  \mc
  \she \to \kahlsx\tensor_{\ox}\she
  \]
  such that for all open subsets $U\sse X$ and $s\in \she(U),f\in\ox(U)$
  \[
  D(f\cdot s)
  =
  \partial(f)\tensor s
  +
  f\cdot D(s)
  \]
  holds,
  where $\partial$ is the canonical map
  \[
  \partial \mc \ox \to \kahlsx.
  \]
\end{defn}
\begin{construction}
  Denote by $\shi\sse \oo_{X\times X}$ the ideal sheaf of the diagonal. Then $\directdelta \ox = \oo_{X\times X}/\shi$, and $\directdelta \kahlsx = \shi/\shi^2$; we denote them by $\ood$ and $\kahlsd$.
  Analogous to the affine case, they fit into the following short-exact sequence of $\ox$-$\ox$-bimoudles:
  \begin{equation}\label{varieties_hkr:atiyah-class-ses1}
  \begin{tikzcd}[column sep = small, cramped]
    0
    \ar{r}
    &
    \kahlsd
    \ar{r}
    &
    \oo_{X\times X}/\shi^2
    \ar{r}
    &
    \ood
    \ar{r}
    &
    0
  \end{tikzcd}.
  \end{equation}
  Let $\she\in \bderivedd(X)$.
  Since all terms in the above sequence are locally free, tensoring with $\she$ as left $\ox$-module leads to the following short exact sequence of right $\ox$-modules (where we don't even need to derive the tensor products):
  \[
  \begin{tikzcd}[column sep = small, cramped]
    0
    \ar{r}
    &
    p_{2\ast}\left(\inverseimage{p}_1 \she \tensor_{\oo_{X\times X}}\right)
    \ar{r}
    &
    p_{2\ast}\left(\inverseimage{p}_1 \she
    \tensor_{\oo_{X\times X}} \oo_{X\times X}/\shi^2\right)
    \ar{r}
    &
    p_{2\ast}\left(\inverseimage{p}_1 \she
    \tensor_{\oo_{X\times X}}\ood
    \right)
    \ar{r}
    &
    0
  \end{tikzcd}.
  \]
  We call the right $\ox$-module
  \[
  \jet(\she)\defined p_{2\ast}\left(\inverseimage{p}_1 \she
  \tensor_{\oo_{X\times X}} \oo_{X\times X}/\shi^2\right)
  \]
  the \emph{sheaf of first jets} on $\she$.
  \glsadd{first-jets}
  Note that we can write this short-exact sequence of $\ox$-modules as
  \[
  \begin{tikzcd}[column sep = small, cramped]
    0
    \ar{r}
    &
    \she\tensor_{\ox}\kahlsx
    \ar{r}
    &
    \she \tensor \oo/\shi^2
    \ar{r}
    &
    \she
    \ar{r}
    &
    0
  \end{tikzcd},
  \]
  and so $\jet(\she)$ defines an element
  \[
  \ati(\she)
  \in
  \ext^1_{X}(\she,\she\tensor \kahlsx),
  \]
  which we call the \emph{Atiyah class} of $\she$.\par
  \glsadd{atiyah-class}
  \coms I don't understand what's going on below thb\come
\end{construction}
We now list some facts (without proof) from the paper, that will be used later.
\begin{lem}
  For $\she,\shf\in \bderivedd(X)$, it holds that
  \[
  \ati(\she\dtensor\shf)
  =
  \ati(\she)\tensor \id
  +
  \id
  \tensor
  \ati(\shf).
  \]
\end{lem}

\begin{lem}
  Consider the Atiyah class of the cotangent bundle
  \[
  \ati(\kahlsx)
  \mc
  \kahlsx
  \to
  \kahlsx \tensor \kahlsx[1].
  \]
  \begin{enumerate}
    \item
    The Atiyah class $\ati(\kahlsx)$ is symmetric, in the sense that the diagram
    \[
    \begin{tikzcd}
      \kahlsx
      \ar{r}[above]{\ati(\kahlsx)}
      \ar{d}[left]{\ati(\kahlsx)}
      &
      \kahlsx\tensor \kahlsx[1]
      \ar{ld}[below right]{\text{flip}}
      \\
      \kahlsx\tensor\kahlsx[1]
    \end{tikzcd}
    \]
    commutes.
    \item
    The Atiyah class obeys the following \emph{Jacobi identity}:
    The projection of
    \[
    \left(\ati(\kahlsx)\tensor \id\right)\circ \ati(\kahlsx)
    \]
    onto the fixed points of the $\mathfrak{S}_3$-action on $\Omega^{1,\tensor 3}_{X}$ is the zero morphism.
    This means that the image of the map
    \[
    \begin{tikzcd}[column sep = large]
      \kahlsx
      \ar{r}[above]{\ati(\kahlsx)}
      &
      \kahlsx\tensor\kahlsx[1]
      \ar{r}[above]{\ati(\kahlsx)\tensor\id}
      &
      \kahlsx\tensor\kahlsx\tensor\kahlsx[2]
    \end{tikzcd}
    \]
    under the canonical projection
    \[
    \begin{tikzcd}[column sep = small, cramped]
    \hom_{\bderivedd(X)}
    (\kahlsx,\Omega^{1,\tensor 3}_{X}[2])
    \ar[twoheadrightarrow]{r}
    &
    (\kahlsx,\Omega^{1,\tensor 3}_{X}[2])^{\mathfrak{S}_3}
    \end{tikzcd}
    \]
    is zero.
  \end{enumerate}
\end{lem}
\begin{construction}
  The above lemma is supposed to imply that there is a ``Lie algebra structure'' on the shifted tangend bundle
  $
  \sht
  \defined
  (\kahlsx)^{\vee}.
  $
  I'm not sure what this is supposed to mean exactly, but I think we're fine if we just ``dualize+shift'' the statement of the lemma to the map
  \[
  [-,-]\defined
  \ati(\kahlsx)^{\vee}[-1]
  \mc
  \sht \dtensor \sht
  \to
  \sht.
  \]
  (\coms I should spell this out more concretely, but I'm a bit in a rush rn\come)
\end{construction}
\begin{defn}
  We say that an algebra $U\in \bderivedd(X)$ with unit $e\in \hom_{\bderivedd(X)}(\ox,U)$ and multiplication $m\mc U\dtensor_{\ox}U\to U$ is the \emph{enveloping algebra} of $\sht$ if the following hold:
  \begin{enumerate}
    \item
    There is a map $\iota\mc \sht\to U$ such that
    \[
    \iota \circ [-,-]
    =
    (m - m^{\sigma})\circ (\iota\tensor\iota)
    \]
    holds as maps
    \[
    \sht \dtensor_{\ox}\sht
    \to
    U
    \].
    (\coms I should figure out what $m^{\sigma}$ is supposed to mean exactly\come)
    \item
    Write
    \[
    \sympow(\sht)
    \defined
    \bigoplus_{j\geq 0}\exterior^j\sht[-j].
    \]
    We then require that the composition
    \[
    \begin{tikzcd}
    \sympow(\sht)
    \ar[hookrightarrow]{r}
    &
    \displaystyle
    \bigoplus_{j \geq 0}\sht^{\tensor j}
    \ar{r}[above]{\bigoplus\iota^{\tensor j}}
    &
    \bigoplus_{j\geq 0}
    U^{\tensor j}
    \ar{r}[above]{\bigoplus m^{\tensor j}}
    &
    \displaystyle
    \bigoplus_{j\geq 0}
    U
    \end{tikzcd}
    \]
    is an isomophism.
  \end{enumerate}
\end{defn}

\begin{construction}
Define
\[
\shu
\defined
\rderived p_{1\ast}
\rinthom_{X\times X}(\ood,\ood).
\]
The the algebra structure of $\ood$ induces an algebra structure on $\shu$ (\coms this needs more details, who would have thought\come).
Let $\pi \mc \Omega^1_{X\times X}\to p_1^{\ast}\kahlsx$ be the natural projection.
This is supposed to induce a map $\iota \mc \sht \to \shu$, \coms but I'm not sure how to read this definition\come.
\end{construction}

\begin{theorem}
  The algebra $\shu$ together with the map $\iota\mc \sht \to \shu$ is the enveloping algebra of $\sht$.
\end{theorem}
\begin{proof}
  The first part is supposed to be calculatalbe.
  For the second part, the paper claims that it suffices to show the claim for $\spec(k[[x_1,\cdots,x_n]])$ (\coms but I'm not sure how this works, again\come).
  Alternativiley, they observe that in the affine case, the morphism reduces to the isomophism for Hochschild cohomology from the affine HKR.
\end{proof}
\begin{construction}
  Recall that we defined an object
  \[
  \shf
  \defined
  \rderived p_{1\ast}(\ood\dtensor \ood) \in \bderivedd(X).
  \]
  There is an action
  \[
  D: \shu \dtensor_{\ox} \shf \to \shf,
  \]
  which is dual to the map
  \[
  \begin{tikzcd}
    \shu
    =
    \rinthom(\ood,\ood)
    \ar{d}[right]{\tensor \id}
    \\
    \rinthom(\ood\dtensor\ood,\ood\dtensor\ood)
    =
    \rinthom(\shf,\shf).
  \end{tikzcd}
  \]
  Composing with the map $\iota\mc \sht \to \shu$ gives a map
  \[
  \begin{tikzcd}[column sep = small]
  \sht \dtensor
  \rderived p_{1\ast}\left(\ood\dtensor\ood\right)
  \ar{r}[above]{D}
  &
  \rderived p_{1\ast}
  \left(\ood\dtensor\ood\right)
  \end{tikzcd}
  \]
  Also note that we have a canonical morphism $\ood\dtensor\ood \to \ood \tensor \ood = \ood$, which gives rise to a morphism $\eepsilon\mc \shf \to \ood$.
\end{construction}
\begin{prop}
  The composition $\eepsilon\circ D$ defines a perfect pairing
  \[
  \shu
  \dtensor
  \shf
  \to \ox.
  \]
  (\coms I'm not sure if they're ommiting a map $\ood \to \oo$, like $\rderived p_1^{\ast}$ or something.
  Also, it feels like this pairing is hidden in some sort of pvdb-duality that allows us to transfer hkr for cohomology to hkr for homology, but i'm not sure whats going on here\come)
\end{prop}
\begin{numtext}
  This proposition allows us to identify $\shf$ as $\shu^{\vee}$, and so we get an induced isomophism
  \[
  E\mc
  \shf
  \isomorphism
  \bigoplus_{j}\Omega^j_{X}[j]
  =
  \left(
  \bigoplus_j \exterior^j T[-j]
  \right)^{\vee}.
  \]
  They then proceed and construct a morphism $L$ that makes the diagram
  \[
  \begin{tikzcd}
  \sht \tensor \shf
  \ar{r}[above]{D}
  \ar{d}[left]{\id \tensor E}
  &
  \shf
  \ar{d}[right]{E}
  \\
  T
  \tensor
  \displaystyle
  \bigoplus_j\Omega^j[j][-1]
  \ar{r}[below]{L}
  &
  \bigoplus_j\Omega^j[j]
  \end{tikzcd}
  \]
  \coms but i don't understand this construction rn
  Now some of this supposed to give HKR for Hochschild homology and coincide with what we know in the affine setting, but what theyre doing is still mysterious to me.\come
\end{numtext}

\subsection{Proof using the Hochschild sheaf}
\begin{numtext}
  I think that it should also be possible to globalize HKR for homology with the Hochschild sheaf, and maybe I'll add this at some point.
\end{numtext}
