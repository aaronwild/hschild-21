\section{Completions}
\label{app_completions}
\begin{numtext}
To understand the power series ring better, we need a notion of ``continuity'' on it.
This is done in the setting of ``completions'', which we will briefly introduce, following \cite{eisenbud_commutative_algebra}*{Chapter 7} and \cite{matsumura}*{§8}.
For power series rings, we will be only interested in viewing it as the ``$t$-adically completion'' of the polynomial ring, and we will try not to get more general.
\end{numtext}
\begin{construction}
  Let $R$ be a ring and $M$ an $R$-module; let $\lset M_i\rset$ be a filtered chain of submodules:
  \[
  M = M_0 \supseteq M_1 \supseteq M_2 \ldots \supseteq 0.
  \]
  Taking this as a system of neighbourhoods of 0 turns $M$ into a topological group under addition, in which scalar multiplication is continous.
  We define the \emph{completion} $\compl{M}$ of $M$ to be the inverse limit over the quotients $M/M_i$:
  \begin{align*}
  \compl{M}
  &\defined
  \varprojlim M/M_i
  \\
  &=
  \lset
  x = \left(x_1,x_2,\ldots\right) \in \prod_i M/M_i \ssp
  x_j \equiv x_i \mod M_i \text{ for all $j>i$}
  \rset
  .
  \end{align*}
  This becomes a topological group by endowing each $M/M_i$ with the discrete topology, the product $\prod_i M/M_i$ with the product topology and then $\compl{M}$ with the subset topology.
  \
  In this setup, the canonical map $M\to \compl{M}$ is continous, and its image is dense in $\compl{M}$.
  A system of neighbourhoods of $\compl{M}$ is given by
  \[
  M_i^{\ast}
  \defined
  \ker
  \left(
  p_i\mc \compl{M} \to M/_i
  \right).
  \]
  \par
  If $M = R$, then $R$ becomes a topological ring.
  If the filtration of $R$ is given by powers of a maximal ideal $\idm$, i.e. $M_i = \idm^i$, then we call $\compl{R}$ the \emph{$\idm$-adically completion}.
  It is a local ring with maximal ideal $\compl{\idm}$.
\end{construction}
\begin{example}
  If $S = R[t_1,\ldots,t_n]$ is a polynomial ring in finitely many variables and $\idm \defined (t_1,\ldots,t_n)$, then the $\idm$-adically completion of $S$ is the formal power series ring in $n$ variables:
  \[
  \compl{S}
  =
  R[[t_1,\ldots,t_n]].
  \]
\end{example}
\begin{construction}
  Let $M$ and $N$ be two topological $R$-modules and let $f\mc M\to N$ be a continous linear map.
  Denote the filtration on $M$ by $\lset M_i\rset$ and the filtration on $N$ by $\lset N_j\rset$.
  Since $f$ is continous, for any $j$ there is an $i$ such that $M_i \sse f^{-1}(N_j)$.
  Define a map $\pphi_j \mc \compl{M}\to N/N_j$ as the composition
  \[
  \begin{tikzcd}[column sep = small, cramped]
    \compl{M}
    \ar[twoheadrightarrow]{r}
    &
    \compl{M}/M_i^{\ast}
    \ar{r}[above]{f}
    &
    N/N_j
  \end{tikzcd}.
  \]
  Then $\pphi_j$ does not depend on $i$, and for all $j'\leq j$, the diagram
  \[
  \begin{tikzcd}
    \compl{M}
    \ar{rr}[above]{\pphi_{j'}}
    \ar{rd}[below left]{\pphi_j}
    &
    &
    N/N_{j'}
    \ar{ld}
    \\
    &
    N/N_j
    &
  \end{tikzcd}
  \]
  commutes.
  Then these $\pphi_j$ assemble into a map $\compl{f}$ that makes the diagram
  \[
  \begin{tikzcd}
    M
    \ar{r}[above]{f}
    \ar{d}
    &
    N
    \ar{d}
    \\
    \compl{M}
    \ar{r}[below]{\compl{f}}
    &
    \compl{N}
  \end{tikzcd}
  \]
  commutative, and $\compl{f}$ is uniquely determined by this commutativity and continuity.
  If $M=R$ and $N=S$ are rings and $f\mc R\to S$ is a ring homomorphism, then $\compl{f}\mc \compl{R}\to\compl{S}$ is a ring homomorphism.
  In particular
\end{construction}
\begin{example}
  As a particular instance of this, the multiplication on the power series ring (\coms prbly also in more cases, this is all we need rn\come) arrises in this way.
  Write again $S = R[t_1,\ldots t_n]$.
  Then ring homomorphism
  \[
  \mu\mc S\times S\to S,~(p,q)\mapsto qp
  \]
  is continous, and hence induces a continous map
  \[
  \compl{\mu}
  \mc
  \compl{S}\times \compl{S}
  \to
  \compl{S},
  \]
  which coincides with the multiplication map on $\compl{S} = R[[t_1,\ldots,t_n]]$.
\end{example}

\begin{defn}[\cite{stacks}*{0AMU}]
  Let $R$ be a topological ring, and let $M$ and $N$ be topological $R$-modules.
  The \emph{tensor product} of $M$ and $N$ is the (usual) tensor product $M\tensor_R N$, endowed with the linear topology induced by the filtratoin
  \[
  \im
  \left(
  M_i \tensor_R N
  +
  M\tensor_R N_j
  \to
  M\tensor_R N
  \right).
  \]
  The \emph{completed tesor product}
  \[
  M
  \ctensor_R
  N
  =
  \lim
  \left(
  M_i\tensor_R N
  +
  M \tensor N_j
  \right)
  =
  \lim
  \left(
  M/M_i
  \tensor
  N/N_j
  \right)
  \]
  is the completion of the tensor product.
  \glsadd{complete-tensor-product}
\end{defn}
The completed tensor product enjoys basically the same properties as its ``ordinary'' counterpart:
\begin{prop}
  Let $R$ be a topological ring.
  \begin{enumerate}
    \item
    Let $M,N$ be topological $R$-modules and $T$ a complete topological $R$-module.
    Endow $M\times R$ with the ordinary product topology.
    Then for every continous $R$-bilinear map $f\mc M \times N \to N$ there is a unique continous $R$-linear map $\compl{f}\mc M \ctensor_R N \to T$ such that the diagram
    \[
    \begin{tikzcd}
      M\times N
      \ar{r}[above]{f}
      \ar{d}
      &
      T
      \\
      M\ctensor_R N
      \ar[dashed]{ur}[below right]{\exists ! \compl{f}}
    \end{tikzcd}
    \]
    commutes.
    \item
    Let $M_1,M_2,N_1,N_2$ be topological $R$-modules and $f_1\mc M_1\to N_1$, $f_2\mc M_2 \to N_2$ be continous $R$-linear maps.
    Then there is a unique continous $R$-linear map $f_1\ctensor f_2\mc M_1\ctensor_R M_2 \to N_1 \ctensor N_2$ such that the diagram
    \[
    \begin{tikzcd}
      M_1\times M_2
      \ar{r}[above]{(f_1,f_2)}
      \ar{d}
      &
      N_1\times N_2
      \ar{d}
      \\
      M_1 \ctensor_R M_2
      \ar{r}[below]{\compl{f}}
      &
      N_1 \ctensor_R N_2
    \end{tikzcd}
    \]
    commutes.
  \end{enumerate}
\end{prop}
\begin{proof}
  \coms I'm not sure if this is actually true lmao \come
\end{proof}
\begin{construction}
  Let $R$ be a topological ring, with topology given by a collection $\lset M_i\rset$.
  Then the \emph{associated graded ring} is defined as
  \[
  \graded_{\lset M_i\rset}(R)
  \defined
  \bigoplus_{i\geq 0}
  M_i/M_{i+1}.
  \]
  If $\lset M_i\rset = \lset \idm^i\rset$ for a maximal ideal $\idm$, then we just write $\graded_{\idm}(R)$ for the associated graded ring, and we will ommit the index altogther when it is not really needed.
  For example, we have
  \[
  \graded_{(t)}R[[t]] = R[t].
  \]
  This assignement should induce a functor
  \[
  R\topmodcat \to \graded(R)\topmodcat,
  \]
  that sends $R$-algebras to $\graded(R)$-algebras and preserves (continous) $R$-algebra maps.
\end{construction}
\begin{prop}
  Let $R$ be a ring, and let $S$ be an $R$-algebra that is $\idn$-adically complete, and let $f_1,\ldots,f_n\in \idn$.
  \begin{enumerate}
    \item
    There is a unique $R$-algebra homomorphism
    \[
    \pphi \mc R[[t_1,\ldots,t_n]]
    \to
    S,
    \]
    sending $t_i$ to $f_i$ for each $i$.
    The map $\pphi$ takes a power series $g(t_1,\ldots,t_n)$ to $g(f_1,\ldots,f_n)\in S$.
    \item
    If the induced map $R\to S/\idn$ is an empimorphism and $f_1,\cdot,f_n$ generate $\idn$, then $\pphi$ in an empimorphism.
    (\coms in what category?\come)
    \item
    If the induced map of associated graded rings
    \[
    \graded(\pphi)
    \mc
    R[t_1,\ldots,t_n]
    \cong
    \graded_{(t_1,\ldots,t_n)}
    R[[t_1,\ldots,t_n]]
    \to
    \graded_{\idn}(S)
    \]
    is a monomorphism, then $\pphi$ is a monomorphism.
  \end{enumerate}
\end{prop}
\begin{cor}
  Let $f\in tR[[t]]$ be a power series that has no constant coefficient.
  Denote by $\pphi$ the endomorphism
  \[
  R[[t]]
  \to
  R[[t]],~
  t\mapsto f
  \]
  then $\pphi$ is an isomorphism if and only if $\partial f(0)$ is a unit in $R$.
\end{cor}
\begin{proof}
  Suppose that $\partial f(0) = u$ is a unit of $R$.
  We have $\graded_{(t)} R[[t]] = R[t]$, and
  \[
  \graded(\pphi)\mc R[t]\to R[t],~t\mapsto ut
  \]
  is an isomorphism because $u$ is a unit.
  Hence $\pphi$ is injective.
  Write \[f= ut + ht^2 = (u+ht)t\] for some $h\in R[[t]]$.
  Since $u$ is a unit in $R$, $u+ht$ is a unit in $R[[t]]$, and so $f$ generates $(t)$.
  So $\pphi$ is surjective, and thus an isomorphism.
  \par
  For the other direction, we observe that $\pphi$ being an isomorphism implies \[\pphi((t)) = (t).\]
  So $f$ is a generator of $(t)$, and hence $f+(t^2)$ generates $(t)/(t^2)$.
  Since $f\equiv \partial f(0)t\mod t^2$, we have that $\partial f(0)$ is indeed a unit in $R$.
\end{proof}
