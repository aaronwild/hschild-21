\begin{construction}\label{definitions:extensions}
\leavevmode
\begin{enumerate}
    \item \label{definitions:extension:induced-structure}
    Let $f\mc E\twoheadrightarrow A$ be a surjective map of $k$-algebras such that $\left(\ker f\right)^2 = 0$. Then $\ker(f)$ becomes an $A$-bimodule in the following way:
    For $a\in A$, choose a lift $e\in f^{-1}(a)$ and set for an $m\in \ker f$:
    \[
    a\cdot m \defined em\text{ and }m\cdot a \defined me.
    \]
    This is well-defined, since for two different lifts $e,e'$, the difference $e-e'$ is an element in $\ker(f)$ and hence
    \[
    (e-e')m,~m(e-e')\in \left(\ker(f)\right)^2 = 0.
    \]
    \item A \emph{square-zero extension} of $A$ by the $A$-module $M$ is now a surjective map of $k$-algebras $E\twoheadrightarrow A$ such that $\left(\ker(f)\right)^2 = 0$ and $\ker(f)\cong M$ holds as $A$-bimodules. Note that any square-zero extension fits into a short-exact sequence of the form
    \[
    0
    \to
    M
    \to
    E
    \twoheadrightarrow
    A
    \to
    0.
    \]
    \item Given two square-zero extensions of $A$ by $M$, say $f\mc E\twoheadrightarrow A$ and $f'\mc E'\twoheadrightarrow A$, we say that they're \emph{equivalent} if there exists an algebra isomophism $\pphi\mc E\to E'$ such that the diagram
    \[
    \begin{tikzcd}
    0
    \ar{r}
    &
    M
    \ar[equal]{d}
    \ar{r}
    &
    E
    \ar{d}[right]{\pphi}[left]{\cong}
    \ar{r}[above]{f}
    &
    A
    \ar[equal]{d}
    \ar{r}
    &
    0
    \\
    0
    \ar{r}
    &
    M
    \ar{r}
    &
    E'
    \ar{r}[below]{f'}
    &
    A
    \ar{r}
    &
    0
    \end{tikzcd}
    \]
    commutes. We denote the set of all square-zero extensions of $A$ by $M$ by $\algext(A,M)$.
    Note that the equivalence relation is again compatible with the addition and scalar multiplication of $k$-algebra maps.
    \glsadd{algext}
  \end{enumerate}
  Now for every square-zero extension $f\mc E\twoheadrightarrow A$ we can choose a section $s\mc A\to E$ such that $E\cong A\oplus M$, in which case the multiplication on $E$ decomposes as
  \[
  (a,m)\cdot (b,n) = (ab, an + mb + g(a,b))
  \]
  where $g\mc A\tensor_k A\to M$ is the $k$-linear map
  \[
  a\tensor b \mapsto s(ab)-s(a)s(b),
  \]
  which we also call a \emph{factor set} (We can see that this decomposition holds by applying the distributivity in $E$ to the summands implicitly involved in this decomposition).
  Conversely, any factor set $g\mc A\tensor_k A\to M$ determines such a surjective map of vector spaces
  \[
  A\oplus M \to A
  \]
  such that $M\cong \ker f$.
  \begin{enumerate}[resume]
    \item As a quick final remark, we can use this splitting decomposition to see that different choices of isomorphisms, say \[\psi_1,\psi_2\mc \ker f\to E\] lead to equivalent extensions, by considering the diagram
    \[
    \begin{tikzcd}[ampersand replacement=\&, column sep = huge]
    0
    \ar{r}
    \&
    \ker f
    \ar[equal]{d}
    \ar{r}[above]{(\psi_1,0)}
    \&
    M\oplus A
    \ar{d}[right]{\begin{pmatrix}\psi_2\circ \psi_1^{-1}&0\\0&\id_A\end{pmatrix}}
    \ar{r}[above]{f}
    \&
    A
    \ar[equal]{d}
    \ar{r}
    \&
    0
    \\
    0
    \ar{r}
    \&
    \ker f
    \ar{r}[above]{(\psi_2,0)}
    \&
    M\oplus A
    \ar{r}[below]{f'}
    \&
    A
    \ar{r}
    \&
    0
    \end{tikzcd}
    \]
  \end{enumerate}
\end{construction}
\begin{prop}\label{definitions:second-hochschild-extenstions}
  The second Hochschild cohomology is given by the vector space of equivalence relations of square zero extensions:
  \[
  \hh^2(A;M) \cong \algext(A,M).
  \]
  Moreover, under this bijection, the zero-equivalence class corresponds to the trivial extension
  \[
  E\cong A\oplus M
  \]
  with the trivial product structure.
\end{prop}
\begin{proof}
  We prove this by using the equivalence of square-zero extensions and factor sets.
  For that, we first show that every factor set $g\mc A\tensor_k\to M$ is a Hochschild cocycle (i.e. in the kernel of the second Hochschild differential) and in a second step that the image of the first Hochschild cocycle agrees with the maps that are identified under the equivalence relation on square-zero extensions.
  \begin{enumerate}
    \item For $a,b,c\in A$ we have:
    \begin{align*}
      \left(\left(a,0\right)\cdot \left(b,0\right)\right)\cdot\left(c,0\right)
      &=
      \left(abc,g\left(a\tensor b\right)c+g\left(ab\tensor c\right)\right)
      \intertext{and}
      \left(a,0\right)\cdot \left(\left(b,0\right)\cdot\left(c,0\right)\right)
      &=
      \left(abc,ag\left(b\tensor c\right)+g\left(a\tensor bc\right)\right).
    \end{align*}
    By the associativity of the multiplication on $g$ these two are equal, and hence
    \[
    ag(b\tensor c) - g(ab\tensor c) + g(a\tensor bc) - g(ab\tensor c) = 0,
    \]
    so $g$ is indeed a Hochschild cocycle.
    \item The map $g$ depends on the choice of a lift $s\mc A\to E$ -- let $s'\mc A\to E$ be another lift for $f\mc E\to A$, and denote by $\rho'$ the corresponding factor set for $s'$.
    Now $s-s'$ is in fact a map $A\to M = \ker f$, and hence
    \begin{align*}
      g'(a,b) - g(a,b)
      &=
      \left(s'(a)s'(b)-s'(ab)\right) -
      \left(s(a)s(b)- s(ab)\right)
      \\
      &=
      s'(a)\left(s'(b)-s(b)\right)
      \\
      &- \left(s'(ab)-s(ab)\right)
      \\
      &+ \left(s'(a)-s(a)\right)s(b)\\
      &=
      d(s-s')(a\tensor b),
    \end{align*}
    and hence $g'-g$ agree up to boundrary.
  \end{enumerate}
\end{proof}
