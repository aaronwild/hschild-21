\section{Deformation Theory for Algebras}
\subsection{$n$-th order deformations and obstructions}
\begin{numtext}
  We saw in \cref{definitions:second-hochschild-extenstions} that the second Hochschild cohomology $\hh^2(M;A)$ classifies square-zero extensions of $M$.
  In the case that $M = A$, these are algebra structures on $A\oplus At$, where $t^2=0$ holds.
  These correspond to $k[t]/t^2$-algebras $E$ such that $E\tensor_{k[t]/t^2}k \cong A$ holds.
  More explicitly, a Hochschild 2-cocylce $\mu\in \hh^2(A;A)$, which we think of as a map
  \[
  \mu\mc A\ktensor A\to A
  \]
   gives an algebra structure on $A\oplus At$ by setting
  \[
  (a,bt)\cdot (a',b't)
  \defined
  (aa', (ab'+ba' + \mu(a\tensor a'))t)
  \]
  If we think of the $k$-module $A\oplus At$ as $R\defined A[t]/t^2$, then this corresponds to a sort of ``augmented'' multiplication on it: Namely, for two elemets $a+bt$ and $a'+b't$, their ordinary product in $R$ is given by
  \begin{align*}
    (a+bt)\cdot(a'+b't) &= aa' + (ab' + ba')t \\
                        &= m(a\tensor a') + (ab'+ba')t,
  \end{align*}
  where
  \[
  m\mc A\ktensor A \to A,~x\tensor y\mapsto xy
  \]
  is the multiplication map on $A$. Now the augementation of the multiplication by the Hoschild 2-cocyle $\mu$ is given by adding the $\mu(a\tensor a')$-term.\par
  In what follows, we want to consider ``higher order augmentation'', in the sense that we want to extend this procedure in a meaningful way to $A[t]/t^n$. For that, we a slightly different notation:
  We denote the ``ordinary'' multiplication on $A[t]$ by $\mu_0$, and the induced multiplication on the quotient by $\omu_0$.
   For a Hochschild 2-cocyle $\mu_1\in \hh^2(A)$ (note that the index is a one!), the induced algebra structure on $R$ can then be denoted as map
  \[
   \omu_0+  \mu_1^0\cdot t\mc R\ktensor R\to R
  \]
  This algebra structure on $R$ is called the \emph{first-order deformation} of $A$ induced by the 2-cocyle $\mu_1$.\footnote{Note the slight change in notation in contrast to the lecture (because it confused me too much in the lecture what ``$\mu_0$'' was supposed to be). We run into the same problem with $\mu_1$ actually. We don't apply it to all of the polynomial by $t$-linear extension, but only to the constant parts! I try to indicate this with a ${}^0$.}
  We already see that $\omu_0+\mu_1$ gives rise to an augmentation of the multiplication on $A[t]/t^n$ for all $n$, which so far is always associative.
  We now want to further perturb this.
  This will happen in two steps --- we first introduce second-order deformations of $A$, and prove show how they relate to the third Hochschild cohomology.
  After that, we extend it to deformations of arbitrary order.
  \end{numtext}
  \begin{construction}
    Let $\mu_2\mc A\ktensor A\to A$ be a $k$-linear map.
    We want to calculate what conditions need to be satisfied by $\mu_2$ such that the map
    \begin{align*}
    \ast\mc A[t]/(t^3)\ktensor A[t]/(t^3)&\to A[t]/(t^3)\\
    a\tensor b &\mapsto \omu_0(a\tensor b)+ \mu_1^0(a\tensor b) t + \mu^0_2(a\tensor b)t^2
    \end{align*}
    endows $R^{(3)}\defined A[t]/(t^3)$ with a associative multiplication.
    So let's start (we omit everything that is related to the associativity of $\omu_0$ in the calculations):
    \begin{align*}
      (a\ast b)\ast c
      &=
      \left(ab + \mu_1^0(a\tensor b)t + \mu_2^0(a\tensor b)t^2\right)\ast c
      \\
      &=
      abc
      \\
      &
      +
      \mu_1^0(ab\tensor c)t +
      \mu_1^0(a\tensor b)ct
      \\
      &
      +
      \mu_2^0(ab\tensor c)t^2
      +
      \mu_1^0(\mu_1^0(a\tensor b)\tensor c)t^2
      +
      \mu_2^0(a\tensor b)ct^2
      \intertext{Note that the $t^3$ vanishes. On the other hand, we have}
      a\ast(b\ast c)
      &=
      a\ast\left(bc+\mu_1^0(b\tensor c)t + \mu_2^0(b\tensor c)t^2\right)
      \\
      &=
      abc
      \\
      &
      +
      \mu_1^0(a\tensor bc)t + a\mu_1(b\tensor c)t
      \\
      &
      +
      \mu_2^0(a\tensor bc)t^2
      +
      \mu_1^0(a\tensor \mu_1^0(b\tensor c))t^2
      +
      a\mu_2^0(b\tensor c)t^2
    \end{align*}
    Equating the linear terms, we  find that the first condition on associativity is given by
    \[
    \mu_1^0(ab\tensor c)
    -
    a\mu_1(b\tensor c)
    +
    \mu_1^0(a\tensor b)c
    -
    \mu_1^0(a\tensor bc)
    =
    0
    \]
    which is equivalent to $\mu_1$ being a Hochschild 2-cycle.
    Turning to the square power, the second condition reads as :
    \begin{align*}
      &\mu_1^0(\mu_1^0(a\tensor b)\tensor c - \mu_1^0(a\tensor \mu_1^0(b\tensor c))
      \\
      &= a\mu_2^0(b\tensor c)t^2 -   \mu_2^0(ab\tensor c)
      + \mu_2^0(a\tensor bc) - \mu_2^0(a\tensor b)c
    \end{align*}
    The right-hand side is precisely $(d\mu_2)^0(a\tensor b\tensor c)$, so in particular, the map $\mu_2$ is a Hochschild coboundary, and the left-hand side equals
    \[
    \mu_1\circ\mu_1 = \frac{1}{2}[\mu_1,\mu_1],
    \]
    where $[,]$ is the bracket from \cref{structure_on_cohomology:hoh-is-gerstenhaber}.
    Putting all of this together, we get:
  \end{construction}
  \begin{prop}
    A first-order deformation $\omu_0+\mu_1^0t$ of $A$ extends to a second-order deformation $\omu_0+\mu_1^0t + \mu_2^0t^2$ if and only if $[\mu_1,\mu_1] = 0$ holds in $\hh^3(A)$.
  \end{prop}
\begin{defn}
  Let $\mu_1\mc A\tensor A\to k$ be part of a first-order deformation.
  We say that the class $[\mu_1,\mu_1]\in \hh^3(A)$ is the \emph{obstruction} to extending $\mu_1$ to a second-order deformation.
  If $[\mu_1,\mu_1] = 0$ we say that $\mu_1$ is \emph{unobstructed}, otherwise that it is \emph{obstructed}.
  We also say that $\hh^3(A)$ is the \emph{obstruction space} of $A$.
\end{defn}
\begin{rem}
  If $\hh^3(A)$ does not vanish, it is still possible that every $\mu_1$ from a first-order deformation is unobstructed.
  %TODO: add example, maybe a Lie algebra
\end{rem}
For $n$-th order deformations, we also have:
\begin{prop}
  Let $\omu_0+\mu_1^0t + \ldots + \mu_n^0 t^n$ be an $n$-th order deformation of $A$.
  Then it can be extended to a $(n+1)$th order deformation of $A$ if and only if
  \begin{align*}
  A\tensor A\tensor A &\to A\\
  a\tensor b\tensor c & \mapsto
  \sum_{i=1}^n\mu_i\left(\mu_{n+1-i}(a\tensor b)\tensor c)- \mu_i(a\tensor \mu_{n+1-i}(b\tensor c)\right)
  \end{align*}
  vanishes in $\hh^3(A)$.
\end{prop}
\begin{proof}
  This follows from the same reasoning as for the second order case, by interpreting the sum above as a sum over Gerstenhaber brackets.
\end{proof}

\subsection{Formal deformations}
\begin{defn}
  A \emph{formal deformation} of $A$ is an assoicative $k[[t]]$-algebra structure $\ast$ on $A[[t]]$ such that
  \[
  A[[t]]\tensor_{k[[t]]} k \cong A
  \]
  holds as $k$-algebras and
  \[
  \left(\sum_{i\geq 0}a_it^i\right)\ast\left(\sum_{j\geq 0}b_jt^j\right)
  =
  \sum_{k\geq 0}\sum_{i+j=k}(a_i\ast b_j)t^{i+j}
  \]
  holds --- so the multiplication $\ast$ satisfies the \emph{Cauchy product formula}.
\end{defn}
Before we talk about how we can obtain formal deformations from $n$-th order deformations, we also introduce the notion of an equivalence of deformations:
\begin{defn}
  Two formal deformations $(A[[t]],\ast)$ and $(A[[t]],\ast')$ (of the same algebra $A$) are \emph{equivalent} if there is a morphism of $k[[t]]$-modules
  $
  \pphi\mc A[[t]]\to A[[t]]
  $
  such that for all $a\in A$, it holds that
  \[
  \pphi(a) = a + \sum_{i\geq 1}\pphi_i(a)t^i
  \]
  for some $k$-linear maps $\pphi_i\mc A\to A$ and
  \[
  \pphi(a\ast b)= \pphi(a)\ast'\pphi(b)
  \]
  holds for all $a,b\in A$.
\end{defn}
\begin{rem}
  In this definition, we do not require the $k[[t]]$-linear map $\pphi$ to be an isomorphism of $k[[t]]$-algebras.
  But this is automatic: (I don't know how to properly phrase the statement ``equivalences of derivations are determined on $A$ and the $\pphi_i$'', because extending this data leads to trouble with infinite sums).
\end{rem}
\begin{construction}
  If we're given a formal deformation on $A$, then the product of two elements $a,b\in A\sse A[[t]]$ is a power series, which we again denote by
  \[
  a\ast b = ab + \mu_1^0(a\tensor b)t + \mu_2^0(a\tensor b)t^2+\ldots
  \]
  This can be extended to all of $A[[t]]$ using the Cauchy product rule.
  \par
  One way of obtaining formal deformations is as ``limits of $n$-th order deformations'':
  First note that we have
  \[ k[[t]] \cong \lim k[t]/(t^n),
  \]
  or, more generally,
  \[
  A[[t]] \cong \lim A[t]/(t^n).
  \]
  Now taking the limit of $n$-th order deformations (which we want to view as maps $A[t]/(t^n)\to A[t]/(t^n)$) gives (by the functoriality of the inverse limit) a formal deformation of $A$.
\end{construction}
\begin{defn}
  We say $(A[[t]],\ast)$ is \emph{trivial} if it is equivalent to $A\tensor_k k[[t]]$.
\end{defn}

\subsubsection*{Alternative definition}
\begin{numtext}
  The definitions for deformations we did in the lecture are still bugging me, because I'm not exactly sure how to correctly extend work with extension of maps from $A$ and tensor products in the setting of power series rings.
  I think the slight modifications that are about to follow should work, but please be extra cautious --- this hasn't been checked by a grown-up yet.
  We use the notations and results of Appendix \ref{app_completions} (and I'm not even sure if everything in there is correct).
\end{numtext}
\begin{defn}
  Let $A$ be a commutative $k$-algebra.
  A \emph{formal deformation} of $A$ is a $k[[t]]$-module structure on $A[[t]]$, together with a continous $k[[t]]$-linear map
  \[
  \ast\mc
  A[[t]]\ctensor_{k[[t]]}A[[t]]
  \to
  A[[t]],
  \]
  such that the composition
  \[
  \begin{tikzcd}[column sep = small, cramped]
  A\tensor_k A
  \ar[hookrightarrow]{r}
  &
  A[[t]]\ctensor_{k[[t]]}A[t]
  \ar{r}[above]{\ast}
  &
  A[[t]]
  \ar[twoheadrightarrow]{r}
  &
  A
  \end{tikzcd}
  \]
  is the usual multiplication on $A$.
  A \emph{morphism of formal deformations} of $A$, say between $\ast$ and $\ast'$, is a $k[[t]]$-linear continous map
  \[
  \pphi\mc A[[t]]\to A[[t]]
  \]
  that is compatible with the maps $\ast,\ast'$ in the sense that the diagram
  \[
  \begin{tikzcd}
    A[[t]]\ctensor_k A[[t]]
    \ar{r}[above]{\ast}
    \ar{d}[left]{\pphi\ctensor\pphi}
    &
    A[[t]]
    \ar{d}[right]{\pphi}
    \\
    A[[t]]\ctensor_k A[[t]]
    \ar{r}[below]{\ast'}
    &
    A[[t]]
  \end{tikzcd}
    \]
    commutes.
    In that way, we obtain the \emph{category of formal deformations} of $A$, which we denote by $\formdef_A$.
\end{defn}

\begin{lem}
  If two formal deformations $\ast$ and $\ast'$ on $A[[t]]$ are equivalant via an equivalence $\pphi$, then their difference is a Hochschild coboundary:
  \[
  \mu_1-\mu_1' = d\pphi_1.
  \]
\end{lem}
\begin{cor}
  If $\ast$ is trivial, then $\mu_1=0$ in $\hh^2(A)$.
\end{cor}
\begin{prop}
  Let $(A[[t]],\ast)$ be a non-trivial formal deformation.
  Then there exists an equivalence $\pphi$ to an algebra structure $\ast'$ and a $n\geq 1$ such that for the maps $\mu_i'\mc A\tensor A\to A$ associated to $\ast'$,
  \[
  \mu_1' = \ldots = \mu_{n-1}' = 0
  \]
  holds in $\zker^2(A)$ and $\mu'_n\neq 0$ holds in $\hh^2(A)$.
\end{prop}
\begin{proof}
  Assume that no such $\pphi$ exists --- then there is a $n\geq 1$ such that
  \[
  \mu_1' = \ldots =  \mu_{n-1}' = 0
  \]
  but $\mu_n\in \bim^2(A)$ is non-zero.
  So we can write
  \[
  a\ast b
  =
  ab
  +
  \mu_n^0(a\tensor b)t^n
  +
  \ldots
  \]
  such that $\mu_n = d\xi$ for some $\xi\in \hohcompg^1(A)$.
  Define
  \begin{align*}
    \pphi(a)
    &
    \defined
    a + \xi(a)t^n,
    \intertext{then this is invertible in $A[[t]]$, with the inverse given by}
    \pphi^{-1}(a)
    &
    =
    a
    +
    \sum_{i\geq 1}(-1)^i\xi^i(a)t^{in}.
  \end{align*}
  We define a new product on $A[[t]]$, by setting
  \[
  a\ast' b
  \defined
  \pphi(\pphi^{-1}(a)\ast\pphi^{-1}(b)).
  \]
  We can then calculate the value of this product on elements $a,b\in A$:
  \begin{align*}
    a\ast' b
    &
    =
    \pphi
    \left(
    \left(
    a-\xi(a)t^n + \ldots
    \right)
    \ast
    \left(
    b-\xi(b)t^n+\ldots
    \right)
    \right)
    \\
    &
    =
    \pphi
    \left(
    ab
    +
    \left(
    \mu_n^0(a\tensor b)
    -
    a\xi(b)
    -
    \xi(a)b
    \right)
    t^n
    +
    \ldots
    \right)
    \\
    &
    =
    ab
    +
    \left(
    \xi(ab)
    +
    \mu_n(a\tensor b)
    -
    a\xi(b)
    -
    \xi(a)b
    \right)
    t^n
    +
    \mu_{n+1}'(a\tensor b)t^{n+1}
    +
    \ldots
    \intertext{and since $\mu_n = d\xi$, we have that the coefficient of $t^n$ vanishes, and hence}
    &
    =
    \mu'_{n+1}(a\tensor b)t^{n+1}+\ldots.
  \end{align*}
  So the degree of the first non-zero ``polynomial'' term of the new $\ast'$-operation is one power higher than what is was for the $\ast$-operation we started with.
  Continuing iterativley, we get maps $\pphi^i\mc A[[t]]\to A[[t]]$, and if this proces doesn't stop, these give an equivalence to the trivial deformation.
\end{proof}

\subsection{Maurer-Cartan equation}
\begin{defn}
  Let $\glie$ be a dg Lie algebra and $\alpha\in \glie^1$.
  The \emph{Maurer-Cartan equation} for $\alpha$ is
  \[
  d\alpha + \frac{1}{2}[\alpha,\alpha] =0,
  \]
  and the \emph{Maurer-Cartan locus} $\mauc(\glie)$ is the set of $\alpha\in \glie^1$ such that the Maurer-Cartan equation holds for $\alpha$:
  \[
  \mauc(\glie)
  \defined
  \lset
  \alpha\in \glie^1 \ssp d\alpha + \frac{1}{2}[\alpha,\alpha] = 0
  \rset.
  \]
  \glsadd{mc-locus}
\end{defn}
\begin{example}
  \leavevmode
  \begin{enumerate}
    \item
    Let $\hohcompg(A)[1]$ be the (shifted) chain complex associated to $A$.
    We motify it to $\glie\defined t\hohcompg(A)[1]$ with $t^2=0$ and a $t$-linear extension of the bracket (so $[at^n,bt^m] = [a,b]t^{n+m}$ etc).
    Then the Maurer-Cartan locus of $\glie$ is just the space of Hochschild 2-cocyles:
    \begin{align*}
    \mauc(\glie)
    &
    =
    \lset
    \alpha t \in \glie^1 \ssp d\alpha + \frac{1}{2}[\alpha t , \alpha t] = 0
    \rset
    &
    \\
    &
    =
    \lset
    \alpha t \in \glie^1 \ssp d\alpha = 0
    \rset
    &
    (t^2 = 0)
    \\
    &
    \cong
    \zker^2(A).
    &
  \end{align*}
  \item
  Same as above, but now we only demand $t^3  =0$.
  Applying the Maurer-Cartan operator to an element
  \[
  \mu_1 t + \mu_2 t^2
  \]
  for Hochschild 1-cochains $\mu_1,\mu_2$ gives
  \[
  d\mu_1 t + d\mu_2 t^2
  +
  \frac{1}{2}
  \left[\mu_1 t + \mu_2 t^2,\mu_1 t + \mu_2 t^2\right]
  \overset{!}{=} 0
  \]
  which is the case if and only if
  \[
  d\mu_1 = 0
  \text{ and }
  d\mu_2 + \frac{1}{2}[\mu_1,\mu_1] = 0
  \]
  This is the case if and only if
  \[
  \mu_1 t + \mu_2 t^2 \]
  induces an associative deformation of $A$ over $k[t]/t^3$.
  \end{enumerate}
\end{example}
\begin{prop}\label{algdeformations:maurer-cartan-bijection}
  The following is a well-defined bijection:
  \begin{align*}
  \lset
  \text{formal deformations of }A
  \rset
  &
  \isomorphism
  \mauc(\hohcompg(A)[1]\tensor_k k[[t]])
  \\
  \mu_0 + \mu_1 t + \mu_2 t^2 + \ldots
  &
  \longmapsto
  \sum_{i\geq 1}\mu_it^i
  \end{align*}
\end{prop}

\subsection{Gauge equivalence}
\begin{defn}
  Two elements $\alpha,\alpha'\in\mauc(\glie)$ are \emph{gauge equivalent} if there is a $\beta\in \glie^0$ such that
  \[
  \alpha'
  =
  \exp(\ad\beta)(\alpha)
  +
  \frac{1-\exp(\ad\beta)}{\ad\beta}(d\beta),
  \]
  where set
  \[
  \frac{1-\exp(\ad\beta)}{\ad\beta}
  \defined
  -
  \sum_{n\geq 0}\frac{\left(\ad\beta\right)^n}{(n+1)!}.
  \]
  Note that we need some sort of nilpotence for the adjoint actions for this to make sense.
\end{defn}
\begin{prop}
  Let $\ast$ and $\ast'$ be formal deformations of $A$.
  Then they are equivalent if and only if their associated Maurer-Cartan elements from \cref{algdeformations:maurer-cartan-bijection} are gauge-equivalent.
\end{prop}

\subsection{Deformation functors}
\begin{defn}
  A \emph{test algebra} over $k$ is a local artinian $k$-algebra $(R,\idm)$ such that $R/\idm \cong k$ holds.
  A \emph{morphism of test algebras} is a local $k$-algebra homomorphism $f\mc R\to S$ (i.e. $f(\idm_R)\sse \idm_S$) holds.
  This gives the \emph{category of test algebras}, which we denote by $\testr_k$.
\end{defn}
\begin{defn}
  Let $A$ be an associative $k$-algebra and $(R,\idm)\in\testrk$ a test algebra.
  A \emph{$R$-deformation} is an associative $R$-bilinear multiplication $\ast$ on $A\tensor_k R$ that reduces to the ``ordinary'' multiplication $\mu_0$ on $A$ (the one coming from the algebra structure we started with) when reducing modulo $\idm$.
  In other words, the following diagram commutes:
  \[
  \begin{tikzcd}
    (A\tensor_k R)
    \tensor_R
    (A\tensor_k R)
    \ar{r}
    \ar{d}[left]{\ast}
    &
    A\tensor_k A
    \ar{d}[right]{\mu_0}
    \\
    A\tensor_k R
    \ar{r}
    &
    A
  \end{tikzcd}
  \]
\end{defn}
